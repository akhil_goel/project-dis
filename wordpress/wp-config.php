<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n`R >T9%J1iAwk3t7X0^yw/WIY!B$j#EbgT38eQrt,?}m-]6:MvV3%{EY]E 0#n3');
define('SECURE_AUTH_KEY',  '+W xjMmfmUL+%1(I+?*4{vg*|k3s,(fKdzqlf6E2yjt}s6C-m`WtcP !7cL`(pta');
define('LOGGED_IN_KEY',    'K/9+~`>:-jr^7O)TQy7{+M(wJ+&<^3eCt]szSIgY9TM-Z.B:jC-H6eB,f  H.5IU');
define('NONCE_KEY',        'Q9j$fA P%C6cl+6=4gmCw%wOLHM=v%0]Yzd+.7GK@DEcE--0nY6@Zwknux{2)#bw');
define('AUTH_SALT',        ']CG?}ef`N?vM]l%[0G|M-kbWDzl%=-j#zNr#DX2]2`r{6+S&K)5k:qjJ}`obo.kH');
define('SECURE_AUTH_SALT', 'sr<m.gtQC;P[pXULpJLHK_d)P#wLl 0>73%q}Md,+lGkD4X9<!pb<hZ^XdI&^=>y');
define('LOGGED_IN_SALT',   '8Hn/v,3a&5YdXc2$.UY3!|HJ84+.|F^QeE(e(M8~y-v~yG-Thv}ji^UOCQZ}LSy8');
define('NONCE_SALT',       '*p-#qfh<7{7aN<%t}i>; (UiN`%>W#GTx@viMR^(=c%5!mbp8f4 E:IWEdFW-boW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
